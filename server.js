﻿require('rootpath')();
const app = require('app/app')

process.env.PORT = process.env.PORT || 4000

// start server
app.listen(process.env.PORT , function () {
  console.log('Server listening on port ' + process.env.PORT)
})