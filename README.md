## Back-end (Node.js / Express / Mongo)

---
### Instalación

```
$ npm install
```
---
### Iniciar server en puerto 4000

```
$ npm start
```
---
### Correr tests y coverage

```
$ npm test
```
---
